---
# Base Preamble Settings
handout:
language:
package:
    -
include-before:
    -

# Title Font Variant
titleformat:
titleformat-title:
titleformat-subtitle:
titleformat-section:
titleformat-frame:

# Section Pages
sectionpage:
subsectionpage:

# Progress Bar
progressbar:

# Color Definitions
color:
    - name:
      type:
      value:

# Color Settings
main-color:
background-color:
title-color:
progressbar-color:
alert-color:
box-color:

# Font Settings
sans-font:
mono-font:

# Frame Numbering
numbering:
    left:
    style:

# Custom Footer Content
footer:
    text:
    logo:

# Pandoc Integration
bold:
quote:
toc:

# Personal Settings
title:
subtitle:
author:
    -
institute:
    -

# Event Information
event:
date:

# Logos
logo:
    -
logo-height:
---
