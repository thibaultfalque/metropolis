# Changelog

This file describes the evolution of the *Metropolis* template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.2.0 (September 2019)

+ Builds can be easily customized by setting `make` variables from the command
  line.
+ Table of contents can be turned on from YAML.

## Version 0.1.0 (August 2019)

+ LaTeX preamble can be customized from YAML.
+ *Metropolis* configuration is supported from YAML.
+ Colors and fonts can be modified from YAML.
+ Institute logo(s) can be put on the title slide by specifying them in YAML.
+ Slide footer can be customized from YAML.
+ Template-specific Pandoc integrations can be turned off from YAML.
+ Document metadata can be set from YAML.
