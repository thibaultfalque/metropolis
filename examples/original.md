---
# Frame Numbering
numbering:
    style: counter

# Custom Footer Content
footer:
    text: Pandoc Template for Metropolis

# Pandoc Integration
bold: true
quote: true
---
