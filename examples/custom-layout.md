---
# Title Font Variant
titleformat-title: allcaps
titleformat-subtitle: allsmallcaps
titleformat-section: smallcaps
titleformat-frame: regular

# Section Pages
sectionpage: simple
subsectionpage: simple

# Progress Bar
progressbar: foot

# Frame Numbering
numbering:
    left: true
    style: fraction

# Custom Footer Content
footer:
    logo: logos/markdown.png

# Pandoc Integration
toc: true
---
