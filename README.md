# Pandoc Template for the *Metropolis* Beamer Theme

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/metropolis/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/metropolis/commits/master)

## Description

This project provides a template allowing an easy customization of the
[*Metropolis* Beamer Theme](https://github.com/matze/mtheme) for a smooth
integration with [Pandoc](https://pandoc.org).

This template, and in particular the bundled [`sty`](./sty) files of
*Metropolis*, is distributed under a Creative Commons Attribution-ShareAlike
4.0 International License.

[![CC-BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

## Requirements

To build your presentation using this template, [Pandoc](https://pandoc.org)
at least 2.0 and [LaTeX](https://www.latex-project.org/) have to be installed
on your computer.

*Metropolis* (as-is from [this](https://github.com/matze/mtheme/commit/2fa6084)
commit) is bundled with this template, so you are not required to install it.

To get the best from this theme, you will need XeLaTeX, which should be bundled
with your LaTeX distribution.
If not, you may want to install it, or to set the variable `PDF_ENGINE` to
`pdflatex` when building (see below).

If you want to have the original font of *Metropolis*, you will need to install
[Mozilla's Fira font](https://github.com/mozilla/Fira).
If this font is not available, an other *sans-serif* font will be used.

## Creating your Presentation

This template enables to write amazing *Metropolis* presentations in plain
[Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), thanks to the
power of Pandoc.

To create your presentation, you will need to put the `sty` directory, the
`Makefile` and the template `metropolis.pandoc` inside the directory in
which you have your Markdown source file and the (optional) metadata file.

The template we provide supports many customizations, so that you can get the
best of *Metropolis* without having to write LaTeX code.

You will find below a description of the available customizations.
You may also be interested in our [examples](./examples), which illustrate
various use cases.

### About the Title Slide

First, note that our template does not use *exactly* the title slide defined by
*Metropolis*.
For an easier customization, logos are put in a table, so that multiple images
can easily be used.
This table is put at the **bottom** of the title slide.

Moreover, this slide is also used as *closing* slide, as commonly advised
for presentations.

### YAML Configuration

Most customizations are set in the YAML configuration.
You are free to put this configuration in a separate file, or at the top of
your Markdown file, provided that you set the variable `METADATA` accordingly
when building.

For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your presentation, and see our
various [examples](./examples) for different use cases.

> *Nota-Bene*
>
> Some variables used in the configuration are used as *Boolean variables*.
> However, Pandoc does not allow to *check* the actual content of a variable.
> As a consequence, we can only check whether a variable exists or not.
>
> This means that, if a variable is set to `false`, it will still be considered
> to be set, and it will not have the expected effect.
>
> That is why you must consider such variables as *flags*, that you set only
> if you need them, preferably to `true` for more readability.
>
> In the following, the description of flag variables always has the form
> "if present, ...".

#### Base Preamble Settings

Some of the base settings can be set with the following variables:

+ `handout`: if present, produces slides without pauses.
+ `language`: sets the language to be used by `babel` (default is `english`).
+ `package`: the list of packages to include in the presentation.
+ `include-before`: the list of all other LaTeX commands you want to add to the
  preamble.

#### *Metropolis* Settings

The *Metropolis* package has a lot of options to help you customize your
presentation.

##### Title Font Variant

You may specify the font variant to use for the various titles of your
presentation with the following variables:

+ `titleformat`: sets the variant for all titles.
+ `titleformat-title`: sets the variant for the title of the presentation.
+ `titleformat-subtitle`: sets the variant for the subtitle of the
  presentation.
+ `titleformat-section`: sets the variant for the title of each section.
+ `titleformat-frame`: sets the variant for the title of each slide.

Note that all these variables take their value among `regular`, `smallcaps`,
`allsmallcaps` and `allcaps`.
Their default value is `regular`.

##### Section Pages

You may add a title slide for each section or subsection by setting the
variables `sectionpage` and `subsectionpage` to one of `none`, `simple`, or
`progressbar`.

By default, `sectionpage` is set to `progressbar` and `subsectionpage` to
`none`.

##### Progress Bar

If you wish, you may put a progress bar on each slide by setting `progressbar`
to `head`, `frametitle`, or `foot`.
The default value is `none` (i.e. there is no progress bar).

#### Color Settings

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

Once you have defined your own colors, you may set the following properties
to customize the colors of your presentation (of course, you may also use
predefined colors):

+ `main-color`: the color for the text and for the background of slide titles.
+ `background-color`: the color for the background of the slides.
+ `title-color`: the color for the slide titles.
+ `progressbar-color`: the color for the progress bars.
+ `alert-color`: the color for alerted text.
+ `box-color`: the background color for boxes, which are used for *punchlines*.

Note that *Metropolis* actually allows to set more colors than the ones we
propose in this template, but we deliberately chose not to use them all, to
avoid having too many different colors on the slides.
Actually, at most five different colors should be used.

#### Font Settings

You may set the sans-serif font and monospace font used in the slides by
setting `sans-font` and `mono-font` accordingly.

#### Slide Footer Settings

Slide footer may contain up to two elements.

##### Frame Numbering

The number of each slide appears at the bottom of the slides.
You may customize this numbering by specifying the two attributes of
the variable `numbering`:

+ `left`: if present, puts the frame numbering at the left of the footer
  (it is put on the right by default).
+ `style`: defines how frame are numbered, among `none`, `counter`, and
  `fraction` (default is `fraction`, and not `counter` as in the original
  *Metropolis*).

##### Custom Footer Content

You may specify your own content for the footer by setting one of the two
attributes of the variable `footer`:

+ `text`: the text to put in the footer.
+ `logo`: the path of the logo to put in the footer (ignored if `text` is set).

This content will appear in the opposite corner of the one used for frame
numbering.

#### Personal Settings

To write your *own* presentation, you need to set the following variables:

+ `title`: the title of the presentation.
+ `subtitle`: the subtitle of the presentation (optional).
+ `author`: the name of the author of the presentation (use a list if there is
  more than one author).
+ `institute`: the name of the author's affiliation (use a list if there is
  more than one affiliation).
+ `event`: the event in which the presentation takes place (optional).
+ `date`: the date on which the presentation takes place (optional, default is
  the current date).

You may also want to add the logo(s) of the affiliation(s) to the title
slide.
This can be achieved by setting `logo` to the list of the images (given by
their path) to use as logos.
If the images are too big, you may also set `logo-height` to change their
height.
The slide width is proportionally divided between each logo.

#### Pandoc Integration

As Markdown does not offer as many features as LaTeX, our template redefines
some styling of *Metropolis*.

First, any **bold** text will actually be interpreted as **alerted** text.
You may restore the original behavior by putting `bold: true` in the metadata.

Second, **quotes** are interpreted as **punchlines**, that is highlighted
blocks of text used as *plot twists* or *take-away* messages.
Once again, you may restore the original behavior by setting `quote: true` in
the metadata.

Last but not least, if you want to add an overview (table of contents) of your
presentation on your second slide, put `toc: true` in the metadata.

### Building your Presentation

Presentations based on this template are built using `make`.
To customize the build to your own use case, you may either update the
[`Makefile`](./Makefile) provided in this project, or set the needed variables
while building.

For example, suppose that your Markdown source file is `example.md`, your
metadata file is `metadata.md` and the titles of your slides correspond to
level-3 titles in your Markdown source file.
You will then have to type the following command to build your presentation:

```bash
make METADATA=metadata.md FILENAME=example SLIDE_LEVEL=3
```

Note that you may also set the program to use to create the PDF from LaTeX
source with `PDF_ENGINE` and customize the way Pandoc produces the slides by
setting the variable `PANDOC_OPTS` accordingly.
For more details, read [Pandoc's User Guide](https://pandoc.org/MANUAL.html).

Our `Makefile` also provides `make clean`, which removes all generated files
but the PDF of the slides, and `make mrproper`, which also removes this PDF.

Type `make help` to have the full list of available targets.
