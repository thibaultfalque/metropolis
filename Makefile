###############################################################################
#     MAKEFILE FOR EASILY BUILDING A METROPOLIS PRESENTATION WITH PANDOC      #
###############################################################################

#############
# VARIABLES #
#############

# The name of the file containing the metadata of the presentation.
# It may be left blank, e.g. if these data are put in the Markdown source file.
METADATA =

# The name of the Markdown source file to build the presentation from.
# Do not put the extension of this file.
# For example, if this file is 'example.md', FILENAME must be set to 'example'.
FILENAME =

# The title level corresponding to the title of the slides.
SLIDE_LEVEL = 1

# The options to pass to Pandoc when converting from Markdown to LaTeX.
# See Pandoc's documentation for more details.
PANDOC_OPTS =

# The engine used to produce the PDF from the LaTeX source.
# Prefer 'xelatex' if you want to get the best from Metropolis.
# Replace it by 'pdflatex' if you do not have 'xelatex'.
PDF_ENGINE = xelatex

# Where to find LaTeX packages, including Metropolis.
# This variable should be left as-is, unless you know what you are doing!
TEXINPUTS = ./sty:$(shell kpsewhich --expand-var='$$TEXINPUTS')

###########
# TARGETS #
###########

# Declares non-file targets.
.PHONY: clean mrproper help

# Builds the presentation from the LaTeX source file.
$(FILENAME).pdf: $(FILENAME).tex
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) -interaction=nonstopmode $(FILENAME).tex
	@export TEXINPUTS=${TEXINPUTS}; $(PDF_ENGINE) -interaction=nonstopmode $(FILENAME).tex

# Converts the Markdown source file into LaTeX.
$(FILENAME).tex: metropolis.pandoc $(METADATA) $(FILENAME).md
	pandoc $(PANDOC_OPTS) --to beamer --template metropolis.pandoc --slide-level $(SLIDE_LEVEL) $(METADATA) $(FILENAME).md --output $(FILENAME).tex

# Removes the auxiliary files used to build the presentation.
clean:
	rm -rf *.aux *.bbl *.blg *.log *.nav *.out *.snm *.tex *.toc *.vrb

# Removes all generated files, including the presentation.
mrproper: clean
	rm -rf $(FILENAME).pdf

# Prints a message describing the available targets.
help:
	@echo
	@echo "Build your Metropolis Presentation with Pandoc"
	@echo "=============================================="
	@echo
	@echo "Available targets are:"
	@echo "    - $(FILENAME).pdf: builds the presentation (default)"
	@echo "    - $(FILENAME).tex: converts from Markdown to LaTeX"
	@echo "    - clean: removes auxiliary files"
	@echo "    - mrproper: removes all generated files"
	@echo "    - help: displays this help"
	@echo
